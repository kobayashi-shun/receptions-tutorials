# トップページ

## Path

/

## 画面

[トップページ](https://docs.google.com/presentation/d/1ClCG7zwb-LPTesBxIUQzqRpXa91yhma60AwE74X0vZY/edit#slide=id.g5a8a9883ef_1_0)

## 仕様

受付システムの最初に表示する画面。

画面全体が、タッチ操作に反応する。  
画面をタッチすると、`会社一覧`に遷移する。

「日本語」ボタンをタッチすると、画面に表示される言語が日本語になる。  
「English」ボタンをタッチすると、画面に表示される言語が英語になる。