# 受付失敗

## Path

/error

## パラメータ

### Post

* httpステータコード

## 画面

[受付失敗](https://docs.google.com/presentation/d/1ClCG7zwb-LPTesBxIUQzqRpXa91yhma60AwE74X0vZY/edit#slide=id.g5acc2d080b_0_0)

## 仕様
  
400番台から500番台のステータスコードが返ってきた際に表示される。
ステータスコードを画面の真ん中に表示する。