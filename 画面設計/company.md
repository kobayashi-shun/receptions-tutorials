# 会社一覧

## Path

/companies

## 画面

[会社一覧](https://docs.google.com/presentation/d/1ClCG7zwb-LPTesBxIUQzqRpXa91yhma60AwE74X0vZY/edit#slide=id.p)  
[会社未登録](https://docs.google.com/presentation/d/1ClCG7zwb-LPTesBxIUQzqRpXa91yhma60AwE74X0vZY/edit#slide=id.g5a9fc036dd_0_3)

## パラメータ

### Get

* 会社名

## 仕様

会社の一覧をボタンで表示する画面。  
DBに会社データが存在しない場合は上記の`会社未登録`を出力する。

一覧の表示は五十音順。  
会社名は一行につき3つずつ表示する。
件数が多い場合はスクロールで表示する。

一覧には以下の項目を表示する。

* 会社名
    * 会社のボタンはリンクになっており、タッチすると`部署一覧`に遷移する。  

担当者検索欄は「担当者名」入力させ、Enterキーをタッチすると`検索結果一覧`に遷移する。  