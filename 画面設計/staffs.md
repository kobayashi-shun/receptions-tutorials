# 担当者一覧

## Path

/companies/{会社ID}/departments/{部署ID}/staffs

## 画面

[担当者一覧](https://docs.google.com/presentation/d/1ClCG7zwb-LPTesBxIUQzqRpXa91yhma60AwE74X0vZY/edit#slide=id.g5a8b64a42e_0_1)  
[担当者未登録](https://docs.google.com/presentation/d/1ClCG7zwb-LPTesBxIUQzqRpXa91yhma60AwE74X0vZY/edit#slide=id.g5a9fc036dd_0_11)

## パラメータ

### Path

* 会社ID
* 部署ID

## 仕様

Pathで渡された会社IDと部署IDをもとに、担当者の一覧をボタンで表示する画面。  
指定された会社ID、部署IDがDBに存在しない場合は`受付失敗`に遷移する。
DBに担当者データが存在しない場合は上記の`担当者未登録`を出力する。

一覧の表示は五十音順。  
担当者は一行につき3つずつ表示する。
件数が多い場合はスクロールで表示する。

一覧には以下の項目を表示する。

* 担当者名
    * 担当者名のボタンはリンクになっており、タッチすると`訪問者情報入力`に遷移する。  

担当者検索欄は「担当者名」入力させ、Enterキーをタッチすると`検索結果一覧`に遷移する。  
フッターには「担当者が不明な場合/上記に担当者がいない場合はこちらをタッチしてください」リンクがあり、タッチすると、`訪問者情報入力(担当者未入力)`に遷移する。