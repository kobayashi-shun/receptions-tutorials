# 部署一覧

## Path

/companies/{会社ID}/departments

## 画面

[部署一覧](https://docs.google.com/presentation/d/1ClCG7zwb-LPTesBxIUQzqRpXa91yhma60AwE74X0vZY/edit#slide=id.g5a8b9f301e_1_1)
[部署未登録](https://docs.google.com/presentation/d/1ClCG7zwb-LPTesBxIUQzqRpXa91yhma60AwE74X0vZY/edit#slide=id.g5a9fc036dd_0_8)

## パラメータ

### Path

* 会社ID

## 仕様

Pathで渡された会社IDをもとに、部署の一覧をボタンで表示する画面。  
指定された会社IDがDBに存在しない場合は`受付失敗`に遷移する。
DBに部署データが存在しない場合は上記の`部署未登録`を出力する。

一覧の表示は五十音順。  
部署名は一行につき3つずつ表示する。
件数が多い場合はスクロールで表示する。

一覧には以下の項目を表示する。

* 部署名
    * 部署のボタンはリンクになっており、タッチすると`担当者一覧`に遷移する。  

担当者検索欄は「担当者名」入力させ、Enterキーをタッチすると`検索結果一覧`に遷移する。  